"""Model for aircraft flight"""


class Flight:

    """
    FLIGHT BOOKING SYSTEM

    '__init__() method'

    This is the initialiser
            - an instance method that is used to initialise values of a newly created object
            - any argument passed to the constructor of a class will be forwarded to the __init__()
              method
            - flight_number should consist of two uppercase letters for airline code and at least three digits
              for route number, otherwise a value error will be raised. String slicing '[FROM : TO]' is used
              to enforce this rule

    """
    def __init__(self, flight_number, aircraft):
        if not flight_number[:2].isalpha():
            raise ValueError("No airline code in '{}'".format(flight_number))

        if not flight_number[:2].isupper():
            raise ValueError("Invalid airline code '{}'".format(flight_number))

        if not flight_number[2:].isdigit() or int(flight_number[2:]) >= 9999:
            raise ValueError("Invalid route number '{}'".format(flight_number))

        self._fl_number = flight_number  #used ' _number' to avoid name clashes with methods of the same name
        self._aircraft = aircraft

        rows, seats = self._aircraft.seating_plan()
        self._seating = [None] + [{letter: None for letter in seats} for _ in rows]

    """
    Returns flight number which is made up of the airline code(two uppercase letters),
    and the route number which is at most 4 digits
    """
    def get_flight_number(self):
        return self._fl_number

    """
    Returns just the airline code which is the first two characters in flight number
    """
    def get_airline_code(self):
        return self._fl_number[:2]
    """
    Returns the aircraft model
    """
    def get_aircraft_model(self):
        return self._aircraft.get_model()

    def _parse_seat(self, seat):
        """
        Parse the seat designator into a row and letter
        :param seat: A seat designator such as 15D
        :return: the seat row and letter
        """
        row_numbers, seat_letters = self._aircraft.seating_plan()

        # negative indexing to get letter which is last character in seat variable
        letter = seat[-1]
        if letter not in seat_letters:
            raise ValueError("Invalid seat letter {}".format(letter))

        # grab the numbers in seat string to check if they are integers using string slicing
        # to discard last character which should be a letter
        row_text = seat[:-1]
        try:
            row = int(row_text)
        except:
            ValueError("Invalid seat row {}".format(row_text))

        # check if row number is within valid range
        if row not in rows:
            raise ValueError("Invalid row number {}".format(row))

        return row, letter

    def allocate_seat(self, seat , passenger):
        """
        :param seat: a seat designator such as 12A, 6F etc
        :param passenger: the passenger name
        :return: nothing

        :raises: ValueError if the seat is unavailable
        """
        row, letter = self._parse_seat(seat)

        if self._seating[row][letter] is not None:
            raise ValueError("Seat {} is already taken ".format(seat))

        #if no errors are raised till this point then assign passenger to the seat
        self._seating[row][letter] = passenger

    def relocate_passenger(self, old_seat, new_seat):
        """
        Relocate passenger to a different seat
        :param old_seat: the current seat the passenger is allocated
        :param new_seat: the seat passenger will me moved to
        :return:
        """
        old_row, old_letter = self._parse_seat(old_seat)
        #check if there really is a passenger to move from the seat
        if self._seating[old_row][old_letter] is None:
            raise ValueError("Seat {} is not occupied, no passenger to relocate".format(old_seat))

        new_row, new_letter = self._parse_seat(new_seat)
        #check if the new seat is not occupied
        if self._seating[new_row][new_letter] is not None:
            raise ValueError("Seat {} is already occupied, cannot move passenger to this seat".format(new_seat))

        #relocate passenger if no errors are raised till this point
        self._seating[new_row][new_letter] = self._seating[old_row][old_letter]
        self._seating[old_row][old_letter] = None


"""
To accept bookings we have to have information about the type of aircraft and some details like
the seating layout and number of available seats
"""
class Aircraft:

    def __init__(self, registration, model, num_rows, num_seats_per_row):
        self._registration = registration
        self._model = model
        self._num_rows = num_rows
        self._num_seats_per_row = num_seats_per_row

    def get_registration(self):
        return self._registration

    def get_model(self):
        return self._model

    def seating_plan(self):
        return (range(1,self._num_rows+1),
                "ABCDEFGHJK"[:self._num_seats_per_row])#string slicing
